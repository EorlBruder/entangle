# SOME DESCRIPTIVE TITLE.
# Copyright (C) YEAR THE PACKAGE'S COPYRIGHT HOLDER
# This file is distributed under the same license as the PACKAGE package.
#
# Translators:
# Robert Antoni Buj Gelonch <rbuj@fedoraproject.org>, 2015. #zanata
# Robert Antoni Buj Gelonch <rbuj@fedoraproject.org>, 2016. #zanata
# Robert Antoni Buj Gelonch <rbuj@fedoraproject.org>, 2017. #zanata
msgid ""
msgstr ""
"Project-Id-Version: entangle\n"
"Report-Msgid-Bugs-To: \n"
"POT-Creation-Date: 2020-06-03 22:07+0100\n"
"PO-Revision-Date: 2017-12-04 06:36+0000\n"
"Last-Translator: Robert Antoni Buj Gelonch <rbuj@fedoraproject.org>\n"
"Language-Team: Catalan (http://www.transifex.com/projects/p/entangle/"
"language/ca/)\n"
"Language: ca\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=2; plural=(n != 1);\n"
"X-Generator: Zanata 4.6.2\n"

msgid "'Escape' to close"
msgstr ""

msgid "0"
msgstr "0"

msgid "1.15:1 - Movietone"
msgstr "1.15:1 - Movietone"

msgid "1.33:1 (4:3, 12:9) - Super 35mm / DSLR / MF 645"
msgstr "1.33:1 (4:3, 12:9) - Súper de 35 mm / DSLR / MF 645"

msgid "1.37:1 - 35mm movie"
msgstr "1.37:1 - Pel·lícula de 35 mm"

msgid "1.44:1 - IMAX"
msgstr "1.44:1 - IMAX"

msgid "1.50:1 (3:2, 15:10)- 35mm SLR"
msgstr "1.50:1 (3:2, 15:10)- SLR de 35 mm"

msgid "1.66:1 (5:3, 15:9) - Super 16mm"
msgstr "1.66:1 (5:3, 15:9) - Súper de 16 mm"

msgid "1.6:1 (8:5, 16:10) - Widescreen"
msgstr "1.6:1 (8:5, 16:10) - Pantalla ampla"

msgid "1.75:1 (7:4) - Widescreen"
msgstr "1.75:1 (7:4) - Pantalla ampla"

msgid "1.77:1 (16:9) - APS-H / HDTV / Widescreen"
msgstr "1.77:1 (16:9) - APS-H / HDTV / Pantalla ampla"

msgid "1.85:1 - 35mm Widescreen"
msgstr "1.85:1 - Pantalla ampla de 35 mm"

msgid "12.00:1 - Circle-Vision 360"
msgstr "12.00:1 - Circle-Vision 360"

msgid "1:1 - Square / MF 6x6"
msgstr "1:1 - Quadrat / MF 6x6"

msgid "2.00:1 - SuperScope"
msgstr "2.00:1 - SuperScope"

msgid "2.10:1 (21:10) - Planned HDTV"
msgstr "2.10:1 (21:10) - HDTV plana"

msgid "2.20:1 (11:5, 22:10) - 70mm movie"
msgstr "2.20:1 (11:5, 22:10) - Pel·lícula de 70 mm"

msgid "2.35:1 - CinemaScope"
msgstr "2.35:1 - CinemaScope"

msgid "2.37:1 (64:27)- HDTV cinema"
msgstr "2.37:1 (64:27)- Cinema HDTV"

msgid "2.39:1 (12:5)- Panavision"
msgstr "2.39:1 (12:5)- Panavision"

msgid "2.55:1 (23:9)- CinemaScope 55"
msgstr "2.55:1 (23:9)- CinemaScope 55"

msgid "2.59:1 (13:5)- Cinerama"
msgstr "2.59:1 (13:5)- Cinerama"

msgid "2.66:1 (8:3, 24:9)- Super 16mm"
msgstr "2.66:1 (8:3, 24:9)- Súper de 16 mm"

msgid "2.76:1 (11:4) - Ultra Panavision"
msgstr "2.76:1 (11:4) - Ultra Panavision"

msgid "2.93:1 - MGM Camera 65"
msgstr "2.93:1 - Càmera 65 de la MGM"

msgid "3"
msgstr "3"

msgid "3:1 APS Panorama"
msgstr "3:1 APS Panorama"

msgid "4.00:1 - Polyvision"
msgstr "4.00:1 - Polyvision"

msgid "<b>Capture</b>"
msgstr "<b>Captura</b>"

msgid "<b>Colour management</b>"
msgstr "<b>Gestió del color</b>"

msgid "<b>Image Viewer</b>"
msgstr "<b>Visualitzador d'imatges</b>"

msgid "<b>Interface</b>"
msgstr "<b>Interfície</b>"

msgid "<b>Plugins</b>"
msgstr "<b>Connectors</b>"

msgid "About - Entangle"
msgstr "Entangle - Quant a"

msgid "Absolute colourimetric"
msgstr "Colorimètric absolut"

msgid "All files (*.*)"
msgstr "Tots els fitxers (*.*)"

msgid "Apply mask to alter aspect ratio"
msgstr "Aplica la màscara per alterar la relació de l'aspecte"

msgid "Aspect ratio:"
msgstr "Relació de l'aspecte:"

msgctxt "shortcut window"
msgid "Autofocus"
msgstr ""

#, c-format
msgid "Autofocus control not available with this camera"
msgstr ""
"El control automàtic de l'enfocament no està disponible per aquesta càmera"

#, c-format
msgid "Autofocus control was not a toggle widget"
msgstr "El control de l'enfocament automàtic no és un giny manipulable"

msgid "Autofocus failed"
msgstr "Error en l'enfocament automàtic"

msgid "Automatically connect to cameras at startup"
msgstr "Connecta la càmera automàticament al començament"

msgid "Automatically synchronize camera clock"
msgstr "Sincronitza automàticament el rellotge de la càmera"

msgid "Automation"
msgstr "Automatització"

msgid "Background:"
msgstr "Fons:"

msgid "Best Fit"
msgstr "Millor ajust"

msgid "Blank screen when capturing images"
msgstr "Pantalla en blanc quan capturi imatges"

msgid "Camera connect failed"
msgstr "Error en connectar amb la càmera"

msgid "Camera control update failed"
msgstr "El control de la càmera no pot actualitzar el camp"

msgid "Camera is in use"
msgstr "La càmera està en ús"

msgid "Camera load controls failed"
msgstr "Error en la càrrega dels controls de la càmera"

msgid "Cancel"
msgstr "Cancel·la"

#, c-format
msgid "Cannot capture image while not opened"
msgstr ""

#, c-format
msgid "Cannot delete file while not opened"
msgstr ""

#, c-format
msgid "Cannot download file while not opened"
msgstr ""

#, c-format
msgid "Cannot initialize gphoto2 abilities"
msgstr "No es poden inicialitzar les habilitats del gphoto2"

#, c-format
msgid "Cannot initialize gphoto2 ports"
msgstr "No es posen inicialitzar els ports del gphoto2"

#, c-format
msgid "Cannot load gphoto2 abilities"
msgstr "No es poden carregar les habilitats del gphoto2"

#, c-format
msgid "Cannot load gphoto2 ports"
msgstr "No es poden carregar els ports del gphoto2"

#, c-format
msgid "Cannot preview image while not opened"
msgstr ""

#, c-format
msgid "Cannot wait for events while not opened"
msgstr ""

msgid "Capture"
msgstr "Captura"

msgctxt "shortcut window"
msgid "Capture a single image"
msgstr "Captura una sola imatge"

msgid "Capture an image"
msgstr "Captura una imatge"

#, c-format
msgid "Capture target setting not available with this camera"
msgstr ""
"Les preferències del destí de la captura no estan disponible per aquesta "
"càmera"

msgid "Capture;Camera;Tethered;Photo;"
msgstr "Captura;Càmera;Tethered;Foto;"

msgid "Center lines"
msgstr "Línies centrals"

msgid ""
"Check that the camera is not\n"
"\n"
" - opened by another photo <b>application</b>\n"
" - mounted as a <b>filesystem</b> on the desktop\n"
" - in <b>sleep mode</b> to save battery power\n"
msgstr ""
"Comproveu que la càmera no estigui\n"
"\n"
" - oberta per una altra <b>aplicació</b> de fotografies\n"
" - muntada com un <b>sistema de fitxers</b> en l'escriptori\n"
" - en <b>mode de repòs</b> per estalviar bateria\n"

msgctxt "shortcut window"
msgid "Close all windows & exit"
msgstr "Tanca totes les finestres i surt"

msgid "Color Management"
msgstr "Gestió del color"

msgid "Colour managed display"
msgstr "Monitor amb gestió del color"

msgid "Continue preview mode after capture"
msgstr "Mode continu de vista prèvia abans de la captura"

msgid "Continuous capture preview"
msgstr "Captura continua de la vista prèvia"

msgctxt "shortcut window"
msgid "Controlling the application"
msgstr "Control de l'aplicació"

#, c-format
msgid "Controls not available for this camera"
msgstr "Els controls no estan disponibles per aquesta càmera"

#, c-format
msgid "Controls not available when camera is closed"
msgstr ""

msgid "Delete"
msgstr "Suprimeix"

msgid "Delete file from camera after downloading"
msgstr "Elimina el fitxer de la càmera després de baixar-ho"

msgid "Detect the system monitor profile"
msgstr "Detecta el perfil del monitor del sistema"

msgid "Display focus point during preview"
msgstr "Mostra el punt focal durant la vista prèvia"

msgctxt "shortcut window"
msgid "Display help manual"
msgstr "Mostra el manual d'ajuda"

msgid "Entangle"
msgstr "Entangle"

msgid "Entangle Preferences"
msgstr "Preferències de l'Entangle"

msgid ""
"Entangle can trigger the camera shutter to capture new images. When "
"supported by the camera, a continuously updating preview of the scene can be "
"displayed prior to capture. Images will be downloaded and displayed as they "
"are captured by the camera. Entangle also allows the settings of the camera "
"to be changed from the computer."
msgstr ""
"Entangle pot disparar l'obturador de la càmera per capturar noves imatges. "
"Quan la càmera és compatible, es pot mostrar abans de la captura una vista "
"prèvia contínua de l'escena. Les imatges es baixen i es mostren com són "
"capturades per la càmera. Entangle també permet que la configuració de la "
"càmera es pugui canviar des de l'ordinador."

msgid ""
"Entangle is a program used to control digital cameras that are connected to "
"the computer via USB."
msgstr ""
"Entangle és un programa que s'utilitza per controlar càmeres digitals que "
"estiguin connectades a l'ordinador a través de l'USB."

msgid ""
"Entangle is compatible with most DSLR cameras from Nikon and Canon, some of "
"their compact camera models, and a variety of cameras from other "
"manufacturers."
msgstr ""
"Entangle és compatible amb la majoria de les càmeres DSLR de Nikon i Canon, "
"alguns dels seus models de càmeres compactes, i una gran varietat de càmeres "
"d'altres fabricants."

msgid "Entangle: Camera autofocus failed"
msgstr "Entangle: error en l'enfocament automàtic de la càmera"

msgid "Entangle: Camera connect failed"
msgstr "Entangle: Error en connectar amb la càmera"

msgid "Entangle: Camera control update failed"
msgstr "Entangle: Error en actualitzar el control de la càmera"

msgid "Entangle: Camera load controls failed"
msgstr "Entangle: Error en la càrrega del control de la càmera"

msgid "Entangle: Camera manual focus failed"
msgstr "Entangle: Error en l'enfocament manual de la càmera"

msgid "Entangle: Operation failed"
msgstr "Entangle: Operació fallida"

#, c-format
msgid "Failed to read manual focus choice %d: %s %d"
msgstr "Error en la lectura de la selecció de l'enfocament manual %d: %s %d"

#, c-format
msgid "Failed to set autofocus state: %s %d"
msgstr "Error en establir l'estat de l'enfocament automàtic: %s %d"

#, c-format
msgid "Failed to set capture target: %s %d"
msgstr "Error en establir el destí de la captura: %s %d"

#, c-format
msgid "Failed to set manual focus state: %s %d"
msgstr "Error en establir l'estat de l'enfocament manual: %s %d"

#, c-format
msgid "Failed to set time state: %s %d"
msgstr "Error en establir l'estat de l'hora: %s %d"

#, c-format
msgid "Failed to set viewfinder state: %s %d"
msgstr "Error en establir l'estat del visor: %s %d"

msgid "Filename pattern:"
msgstr "Patró del nom del fitxer:"

msgid "Flip horizontally"
msgstr ""

msgid "Flip vertically"
msgstr ""

msgctxt "shortcut window"
msgid "Focus control (during live preview)"
msgstr ""

msgctxt "shortcut window"
msgid "Focus in (large step)"
msgstr ""

msgctxt "shortcut window"
msgid "Focus in (small step)"
msgstr ""

msgctxt "shortcut window"
msgid "Focus out (large step)"
msgstr ""

msgctxt "shortcut window"
msgid "Focus out (small step)"
msgstr ""

msgctxt "shortcut window"
msgid "Fullscreen"
msgstr "Pantalla completa"

msgid "Golden sections"
msgstr "Seccions dorades"

msgid "Grid lines:"
msgstr "Línies de quadrícula:"

msgid "Highlight areas of overexposure"
msgstr ""

msgid "Highlight:"
msgstr "Ressaltat:"

msgid "ICC profiles (*.icc, *.icm)"
msgstr "Perfils ICC (*.icc, *.icm)"

msgid "IP Address:"
msgstr "Adreça IP:"

msgid "Image Viewer"
msgstr "Visualitzador d'imatges"

msgctxt "shortcut window"
msgid "Image display"
msgstr "Visualització de la imatge"

msgid "Image histogram"
msgstr "Histograma de la imatge"

msgid "Interface"
msgstr "Interfície"

#, c-format
msgid "Manual focus control not available with this camera"
msgstr ""
"El control de l'enfocament manual no està disponible per aquesta càmera"

#, c-format
msgid "Manual focus control was not a range or radio widget"
msgstr ""
"El control de l'enfocament manual no està dins de l'interval o de la ràtio "
"del giny"

msgid "Manual focus failed"
msgstr "Error en l'enfocament manual"

msgid "Mask opacity:"
msgstr "Màscara d'opacitat:"

msgid "Missing 'execute' method implementation"
msgstr "Falta la implementació del mètode «execute»"

msgid "Mode of operation:"
msgstr "Mode d'operació:"

msgid "Model"
msgstr "Model"

msgid "Monitor"
msgstr "Monitor"

msgid "Monitor profile:"
msgstr "Perfil del monitor:"

msgid "Network camera"
msgstr "Càmera de xarxa"

msgid "No"
msgstr "No"

msgid "No camera connected"
msgstr "Ni hi ha cap càmera connectada"

msgid ""
"No cameras were detected, check that\n"
"\n"
"  - the <b>cables</b> are connected\n"
"  - the camera is <b>turned on</b>\n"
"  - the camera is in the <b>correct mode</b>\n"
"  - the camera is a <b>supported</b> model\n"
"\n"
"USB cameras are automatically detected\n"
"when plugged in, for others try a refresh\n"
"or enter a network camera IP address"
msgstr ""
"No s'ha detectat cap càmera, comproveu que\n"
"\n"
"  - els <b>cables</b> estiguin connectats\n"
"  - la càmera estigui <b>engegada</b>\n"
"  - la càmera estigui en <b>mode de connexió</b>\n"
"  - la càmera sigui <b>compatible</b>\n"
"\n"
"Les càmeres USB es detecten automàticament\n"
"quan s'endollen, per a les altres proveu amb\n"
"actualitzar o introduir l'IP d'una càmera de xarxa"

msgid "No config options"
msgstr "Sense opcions de configuració"

msgid "No controls available"
msgstr "No hi ha cap control disponible"

msgid "No script"
msgstr "Sense script"

msgid "None"
msgstr "Cap"

msgid "Off"
msgstr "Desactivada"

msgid "On"
msgstr "Activada"

msgid "Open"
msgstr "Obre"

msgctxt "shortcut window"
msgid "Open a new camera window"
msgstr "Obre en una finestra de càmera nova"

msgid "Open with"
msgstr "Obre amb"

#, c-format
msgid "Operation: %s"
msgstr "Operació: %s"

msgid "Overlay earlier images"
msgstr "Superposa amb les primeres imatges"

msgid "Overlay layers:"
msgstr "Capes superposades:"

msgid "Pattern must contain 'XXXXXX' placeholder"
msgstr ""

msgid "Perceptual"
msgstr "Perceptiu"

msgid "Plugins"
msgstr "Connectors"

msgid "Port"
msgstr "Port"

msgctxt "shortcut window"
msgid "Presentation"
msgstr "Presentació"

msgid "Preview"
msgstr "Vista prèvia"

msgid "Quarters"
msgstr "Quarts"

msgid "RGB profile:"
msgstr "Perfil RGB:"

msgid "Relative colourimetric"
msgstr "Colorimètric relatiu"

msgid "Rendering intent:"
msgstr "Propòsit de la conversió:"

msgid "Reset controls"
msgstr "Restableix els controls"

msgid "Retry"
msgstr "Reintent"

msgid "Rule of 3rds"
msgstr "Regla del terç"

msgid "Rule of 5ths"
msgstr "Regla del cinquè"

msgid "Saturation"
msgstr "Saturació"

msgid "Screen blanking is not available on this display"
msgstr "La pantalla en blanc no està disponible en aquest monitor"

msgid "Screen blanking is not implemented on this platform"
msgstr "La pantalla en blanc no està implementada en aquesta plataforma"

msgid "Script"
msgstr "Script"

msgid "Select a camera to connect to:"
msgstr "Seleccioneu una càmera a la qual connectar-vos:"

msgid "Select a folder"
msgstr "Selecciona una carpeta"

msgid "Select application..."
msgstr "Selecciona una aplicació..."

msgid "Select camera - Entangle"
msgstr "Entangle - Selecciona la càmera"

msgid "Set clock"
msgstr "Estableix el rellotge"

msgid "Show linear histogram"
msgstr "Mostra l'histograma lineal"

msgctxt "shortcut window"
msgid "Shutter control"
msgstr ""

msgid "Tear off"
msgstr ""

msgid "Tethered Camera Control & Capture"
msgstr "Controleu la càmera enllaçada i captureu"

msgid "Tethered Camera Control &amp; Capture"
msgstr "Controleu la càmera enllaçada i captureu"

msgid "The Entangle Photo project"
msgstr ""

msgid ""
"The camera cannot be opened because it is mounted as a filesystem. Do you "
"wish to umount it now?"
msgstr ""

msgid "This camera does not support image capture"
msgstr "Aquesta càmera no és compatible amb la captura d'imatges"

msgid "This camera does not support image preview"
msgstr "Aquesta càmera no és compatible amb la vista prèvia de la imatge"

#, c-format
msgid "Time setting not available with this camera"
msgstr "L'establiment de l'hora no està disponible en aquesta càmera"

#, c-format
msgid "Time setting was not a choice widget"
msgstr "L'establiment de l'hora no és una opció del giny"

#, c-format
msgid "Time setting was not a date widget"
msgstr "L'establiment de l'hora no és una opció del giny"

msgctxt "shortcut window"
msgid "Toggle histogram scale"
msgstr ""

msgctxt "shortcut window"
msgid "Toggle image mask"
msgstr ""

msgctxt "shortcut window"
msgid "Toggle live preview"
msgstr "Commuta la vista prèvia en directe"

msgctxt "shortcut window"
msgid "Toggle overexposure highlighting"
msgstr ""

#, c-format
msgid "Unable to capture image: %s"
msgstr "No es pot capturar la imatge: %s"

#, c-format
msgid "Unable to capture preview: %s"
msgstr "No es pot capturar la vista prèvia: %s"

#, c-format
msgid "Unable to connect to camera: %s"
msgstr "No es pot connectar amb la càmera: %s"

#, c-format
msgid "Unable to delete file: %s"
msgstr "No es pot eliminar el fitxer: %s"

#, c-format
msgid "Unable to fetch camera control configuration: %s"
msgstr "No s'ha pogut obtenir la configuració del control de la càmera: %s"

#, c-format
msgid "Unable to fetch widget name"
msgstr "No es pot obtenir el nom del giny"

#, c-format
msgid "Unable to fetch widget type"
msgstr "No es pot obtenir el tipus de giny"

#, c-format
msgid "Unable to get camera file: %s"
msgstr "No es pot obtenir el fitxer de la càmera: %s"

#, c-format
msgid "Unable to get file data: %s"
msgstr "No es poden obtenir les dades del fitxer: %s"

#, c-format
msgid "Unable to get filename: %s"
msgstr "No es pot obtenir el nom del fitxer: %s"

#, c-format
msgid "Unable to initialize camera: %s"
msgstr "No es pot inicialitzar la càmera: %s"

#, c-format
msgid "Unable to load controls, camera is not opened"
msgstr ""

#, c-format
msgid "Unable to save camera control configuration: %s"
msgstr "No s'ha pogut desar la configuració del control de la càmera: %s"

#, c-format
msgid "Unable to save controls, camera is not configurable"
msgstr "No es poden desar els controls, la càmera no es pot configurar"

#, c-format
msgid "Unable to save controls, camera is not opened"
msgstr ""

#, c-format
msgid "Unable to wait for events: %s"
msgstr "No es pot esperar per esdeveniments: %s"

msgid "Untitled script"
msgstr "Script sense retolar"

msgid "Use embedded preview from raw files"
msgstr "Utilitza la vista prèvia incrustada dels fitxers raw"

msgid "Use preview output as capture image"
msgstr "Utilitza la sortida de vista prèvia com a imatge de captura"

#, c-format
msgid "Viewfinder control not available with this camera"
msgstr "El control del visor no està disponible en aquesta càmera"

#, c-format
msgid "Viewfinder control was not a toggle widget"
msgstr "El control del visor no és un giny manipulable"

msgctxt "shortcut window"
msgid "Window display"
msgstr "Visualització de la finestra"

msgid "Yes"
msgstr "Sí"

msgid "Zoom _In"
msgstr "_Augmenta el zoom"

msgid "Zoom _Out"
msgstr "_Disminueix el zoom"

msgctxt "shortcut window"
msgid "Zoom best"
msgstr "Millor zoom"

msgctxt "shortcut window"
msgid "Zoom in"
msgstr "Augmenta el zoom"

msgctxt "shortcut window"
msgid "Zoom normal"
msgstr "Zoom normal"

msgctxt "shortcut window"
msgid "Zoom out"
msgstr "Disminueix el zoom"

msgid "_About"
msgstr "Quant _a"

msgid "_Cancel"
msgstr "_Cancel·la"

msgid "_Connect…"
msgstr "_Connecta..."

msgid "_Disconnect…"
msgstr "_Desconnecta..."

msgid "_Fullscreen"
msgstr "_Pantalla completa"

msgid "_Help"
msgstr "A_juda"

msgid "_Keyboard shortcuts"
msgstr "_Dreceres de teclat"

msgid "_Manual"
msgstr ""

msgid "_New window"
msgstr "Finestra _nova"

msgid "_Normal Size"
msgstr "Mida _normal"

msgid "_Open"
msgstr "_Obre"

msgid "_Preferences…"
msgstr "_Preferències..."

msgid "_Presentation"
msgstr "_Presentació"

msgid "_Quit"
msgstr "_Surt"

msgid "_Select session…"
msgstr "_Selecciona la sessió..."

msgid "_Settings"
msgstr "Ajust_s"

msgid "_Supported cameras"
msgstr "Càmere_s compatibles"

msgid "_Synchronize capture"
msgstr "_Sincronitza la captura"

msgid "capture"
msgstr "captura"

msgid "label"
msgstr "etiqueta"

msgid "preview"
msgstr "vista prèvia"

msgid "settings"
msgstr "preferències"

#~ msgid ""
#~ "The camera cannot be opened because it is currently mounted as a "
#~ "filesystem. Do you wish to umount it now ?"
#~ msgstr ""
#~ "La càmera no pot obrir-se perquè actualment està muntada com a sistema de "
#~ "fitxers. Voleu desmuntar-la ara?"

#~ msgid "Cannot capture image while not connected"
#~ msgstr "No es pot capturar una imatge quan no està connectat"

#~ msgid "Cannot preview image while not connected"
#~ msgstr ""
#~ "No es pot realitzar la imatge de vista prèvia quan no està connectat"

#~ msgid "Cannot download file while not connected"
#~ msgstr "No es pot baixar un fitxer quan no està connectat"

#~ msgid "Cannot delete file while not connected"
#~ msgstr "No es pot eliminar un fitxer quan no està connectat"

#~ msgid "Cannot wait for events while not connected"
#~ msgstr "No es pot esperar per esdeveniments quan no està connectat"

#~ msgid "Unable to load controls, camera is not connected"
#~ msgstr "No es poden carregar els controls, la càmera no està connectada"

#~ msgid "Unable to save controls, camera is not connected"
#~ msgstr "No es poden desar els controls, la càmera no està connectada"

#~ msgid "Controls not available when camera is disconnected"
#~ msgstr "Els controls no estan disponibles quan la càmera està desconnectada"

#~ msgid "entangle"
#~ msgstr "entangle"
